﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RTP.CP.Tests
{
    [TestFixture]
    public class DvhCurveAndRoiTypeConfigBuilderTests
    {
        private DVHCurve _dvhCurveWithPercentVolumes;
        private DVHCurve _secondDvhCurveWithCcmVolumes;

        [SetUp]
        public void SetUp()
        {
            _dvhCurveWithPercentVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(50, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.percent)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL, new UVolume(80, UVolume.VolumeUnit.ccm));

            _secondDvhCurveWithCcmVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(50, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(8, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
        }

        [Test]
        public void Build_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("Bowel Small", RtRoiInterpretedTypePresets.SMALL_BOWEL())
                .WithStructureIdAndRoiType("PTV", RtRoiInterpretedTypePresets.PTV())
                .Build();

            var dvhEstimates = new UDvhEstimateDetached[]
            {
                new UDvhEstimateDetached("P1", "Bowel Small", 1, _dvhCurveWithPercentVolumes.GetMinimumDose(),
                    _dvhCurveWithPercentVolumes.GetMeanDose(), _dvhCurveWithPercentVolumes.GetMedianDose(),
                    _dvhCurveWithPercentVolumes.GetMaximumDose(), 1,
                    _dvhCurveWithPercentVolumes.GetStandardDeviationDose(), _dvhCurveWithPercentVolumes.TotalVolume(),
                    _dvhCurveWithPercentVolumes),
                new UDvhEstimateDetached("P1", "PTV", 1, _secondDvhCurveWithCcmVolumes.GetMinimumDose(),
                    _secondDvhCurveWithCcmVolumes.GetMeanDose(), _secondDvhCurveWithCcmVolumes.GetMedianDose(),
                    _secondDvhCurveWithCcmVolumes.GetMaximumDose(), 1,
                    _secondDvhCurveWithCcmVolumes.GetStandardDeviationDose(),
                    _secondDvhCurveWithCcmVolumes.TotalVolume(), _secondDvhCurveWithCcmVolumes),
            };

            var result = DvhCurveAndRoiTypeConfigBuilder.Initialize()
                .WithDvhEstimates(dvhEstimates)
                .WithStructureIdAndRoiTypeDictionary(dict)
                .Build();

            var expectedResult1 =
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.SMALL_BOWEL());
            var expectedResult2 =
                new DVHCurveAndROITypeTuple(_secondDvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.PTV());
            Assert.Contains(expectedResult1, result.DvhCurveAndRoiTypeTuples.ToList());
            Assert.Contains(expectedResult2, result.DvhCurveAndRoiTypeTuples.ToList());
        }

        [Test]
        public void Build_Throw_StructureWithIdNotContainedInStructureIdAndRoiTypeDictionaryException_Test()
        {
            var dict = StructureIdAndRoiTypeDictionaryBuilder.Initialize()
                .WithStructureIdAndRoiType("PTV", RtRoiInterpretedTypePresets.PTV())
                .Build();

            var dvhEstimates = new UDvhEstimateDetached[]
            {
                new UDvhEstimateDetached("P1", "Bowel Small", 1, _dvhCurveWithPercentVolumes.GetMinimumDose(),
                    _dvhCurveWithPercentVolumes.GetMeanDose(), _dvhCurveWithPercentVolumes.GetMedianDose(),
                    _dvhCurveWithPercentVolumes.GetMaximumDose(), 1,
                    _dvhCurveWithPercentVolumes.GetStandardDeviationDose(), _dvhCurveWithPercentVolumes.TotalVolume(),
                    _dvhCurveWithPercentVolumes),
            };

            Assert.Throws<StructureWithIdNotContainedInStructureIdAndRoiTypeDictionaryException>(
                () => DvhCurveAndRoiTypeConfigBuilder.Initialize()
                .WithDvhEstimates(dvhEstimates)
                .WithStructureIdAndRoiTypeDictionary(dict)
                .Build());
        }

        [Test]
        public void Build_Throw_NullReferenceException_IfDictIsNotDefined_Test()
        {
            var dvhEstimates = new UDvhEstimateDetached[]
            {
                new UDvhEstimateDetached("P1", "Bowel Small", 1, _dvhCurveWithPercentVolumes.GetMinimumDose(),
                    _dvhCurveWithPercentVolumes.GetMeanDose(), _dvhCurveWithPercentVolumes.GetMedianDose(),
                    _dvhCurveWithPercentVolumes.GetMaximumDose(), 1,
                    _dvhCurveWithPercentVolumes.GetStandardDeviationDose(), _dvhCurveWithPercentVolumes.TotalVolume(),
                    _dvhCurveWithPercentVolumes),
            };

            Assert.Throws<NullReferenceException>(
                () => DvhCurveAndRoiTypeConfigBuilder.Initialize()
                .WithDvhEstimates(dvhEstimates)
                .Build());
        }

        [Test]
        public void WithDvhEstimates_ForNullArguments_Test()
        {
            Assert.Throws<ArgumentNullException>(
                () => DvhCurveAndRoiTypeConfigBuilder.Initialize()
                .WithDvhEstimates(null));
        }

        [Test]
        public void WithStructureIdAndRoiTypeDictionary_ForNullArguments_Test()
        {
            Assert.Throws<ArgumentNullException>(
                () => DvhCurveAndRoiTypeConfigBuilder.Initialize()
                .WithStructureIdAndRoiTypeDictionary(null));
        }
    }
}
