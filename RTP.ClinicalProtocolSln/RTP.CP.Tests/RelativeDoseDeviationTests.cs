﻿using System;
using NUnit.Framework;

namespace RTP.CP.Tests
{
    [TestFixture]
    class RelativeDoseDeviationTests
    {
        [Test]
        public void Constructor_ThrowArgumentOutOfRangeException_IfValueIsLowerZero_TEst()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new RelativeDoseDeviation(-1));
        }
    }
}
