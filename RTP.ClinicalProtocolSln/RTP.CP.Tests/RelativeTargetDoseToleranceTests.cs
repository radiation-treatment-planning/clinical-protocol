﻿using System;
using NUnit.Framework;

namespace RTP.CP.Tests
{
    [TestFixture]
    class RelativeTargetDoseToleranceTests
    {
        [Test]
        public void Equals_Test()
        {
            var tolerance1 = new RelativeTargetDoseTolerance(30.2);
            var tolerance2 = new RelativeTargetDoseTolerance(40.5);
            var tolerance3 = new RelativeTargetDoseTolerance(30.2);

            Assert.AreEqual(tolerance1, tolerance3);
            Assert.AreNotEqual(tolerance1, tolerance2);
            Assert.AreNotEqual(tolerance3, tolerance2);
        }

        [Test]
        public void Constructor_ThrowArgumentOutOfRangeException_IfValueIsLowerZeroOrHigher100_Test()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new RelativeTargetDoseTolerance(-1));
            Assert.Throws<ArgumentOutOfRangeException>(() => new RelativeTargetDoseTolerance(1001));
        }
    }
}
