﻿using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RTP.CP.Tests
{
    [TestFixture]
    public class ClinicalProtocolTests
    {
        [Test]
        public void Equals_ReturnTrueIfTrue_Test()
        {
            var constraint1 = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);
            var constraint2 = new MinimumClinicalProtocolItem(new UDose(7, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Soft);
            var constraint3 = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Hard);
            var constraint4 = new MaximumClinicalProtocolItem(new UDose(4, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Soft);
            var constraint5 = new MinimumDoseClinicalProtocolItem(new UDose(0.001, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Hard);
            var constraint6 = new MinimumDoseClinicalProtocolItem(new UDose(3, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Soft);
            var constraint7 = new MaximumDoseClinicalProtocolItem(new UDose(16, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Hard);
            var constraint8 = new MinimumDoseClinicalProtocolItem(new UDose(13, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Soft);

            var items = new IClinicalProtocolItem[]
            {
                constraint1, constraint2, constraint3, constraint4, constraint5, constraint6, constraint7,
                constraint8
            };

            var clinicalProtocol = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(items)
                .Build("CP", "Description");
            var clinicalProtocol2 = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(items)
                .Build("CP", "Description");

            Assert.AreEqual(clinicalProtocol, clinicalProtocol2);
        }

        [Test]
        public void Equals_ReturnFalse_IfIdDiffers_Test()
        {
            var constraint = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);

            var items = new IClinicalProtocolItem[] { constraint };

            var clinicalProtocol = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(items)
                .Build("CP", "Description");

            var clinicalProtocol2 = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(items)
                .Build("CP different", "Description");

            Assert.AreNotEqual(clinicalProtocol, clinicalProtocol2);
        }

        [Test]
        public void Equals_ReturnFalse_IfDescriptionDiffers_Test()
        {
            var constraint = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);

            var items = new IClinicalProtocolItem[] { constraint };

            var clinicalProtocol = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(items)
                .Build("CP", "Description");

            var clinicalProtocol2 = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(items)
                .Build("CP", "Description different");

            Assert.AreNotEqual(clinicalProtocol, clinicalProtocol2);
        }

        [Test]
        public void Equals_ReturnFalseIfOneProtocolItemDiffers_Test()
        {
            var constraint1 = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);
            var constraint2 = new MinimumClinicalProtocolItem(new UDose(7, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Soft);
            var constraint3 = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Hard);
            var constraint4 = new MaximumClinicalProtocolItem(new UDose(4, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Soft);
            var constraint5 = new MinimumDoseClinicalProtocolItem(new UDose(0.001, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Hard);
            var constraint6 = new MinimumDoseClinicalProtocolItem(new UDose(3, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Soft);
            var constraint7 = new MaximumDoseClinicalProtocolItem(new UDose(16, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Hard);
            var constraint8_1 = new MinimumDoseClinicalProtocolItem(new UDose(13, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Soft);
            var constraint8_2 = new MinimumDoseClinicalProtocolItem(new UDose(13, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.LIVER(), ConstraintType.Soft);

            var items = new IClinicalProtocolItem[]
            {
                constraint1, constraint2, constraint3, constraint4, constraint5, constraint6, constraint7,
                constraint8_1
            };
            var itemsDifferent = new IClinicalProtocolItem[]
            {
                constraint1, constraint2, constraint3, constraint4, constraint5, constraint6, constraint7,
                constraint8_2
            };

            var clinicalProtocol = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(items)
                .Build("CP", "Description");

            var clinicalProtocol2 = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(itemsDifferent)
                .Build("CP", "Description");

            Assert.AreNotEqual(clinicalProtocol, clinicalProtocol2);
        }
    }
}
