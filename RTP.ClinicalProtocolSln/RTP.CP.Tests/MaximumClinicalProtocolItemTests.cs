﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RTP.CP.Tests
{
    [TestFixture]
    public class MaximumClinicalProtocolItemTests
    {
        private DVHCurve _dvhCurveWithPercentVolumes;
        private DVHCurve _dvhCurveWithCcmVolumes;
        private DVHCurve _secondDvhCurveWithCcmVolumes;

        [SetUp]
        public void SetUp()
        {
            _dvhCurveWithPercentVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(50, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.percent)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            _dvhCurveWithCcmVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(80, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(40, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(8, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            _secondDvhCurveWithCcmVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(50, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(8, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
        }

        [Test]
        public void EquateVolumesOfProtocolItemAndDvhCurve_SameVolumeUnits_Test()
        {
            var clinicalProtocolItem = new MaximumClinicalProtocolItem(new UDose(12, UDose.UDoseUnit.Gy),
                new UVolume(5, UVolume.VolumeUnit.percent), new RtRoiInterpretedType("placeholder", "placeholder"),
                ConstraintType.Hard);

            var (convertedClinicalProtocolItem, convertedDvhCurve) =
                clinicalProtocolItem.EquateVolumesOfProtocolItemAndDvhCurve(_dvhCurveWithPercentVolumes);

            Assert.AreEqual(clinicalProtocolItem, convertedClinicalProtocolItem);
            Assert.AreEqual(_dvhCurveWithPercentVolumes, convertedDvhCurve);
        }

        [Test]
        public void EquateVolumesOfProtocolItemAndDvhCurve_ConvertDvhCurveVolumeToPercent_Test()
        {
            var clinicalProtocolItem = new MaximumClinicalProtocolItem(new UDose(12, UDose.UDoseUnit.Gy),
                new UVolume(5, UVolume.VolumeUnit.percent), new RtRoiInterpretedType("placeholder", "placeholder"),
                ConstraintType.Hard);

            var (convertedClinicalProtocolItem, convertedDvhCurve) =
                clinicalProtocolItem.EquateVolumesOfProtocolItemAndDvhCurve(_dvhCurveWithCcmVolumes);

            Assert.AreEqual(clinicalProtocolItem, convertedClinicalProtocolItem);
            Assert.AreEqual(_dvhCurveWithPercentVolumes, convertedDvhCurve);
        }

        [Test]
        public void EquateVolumesOfProtocolItemAndDvhCurve_ConvertClinicalProtocolItemVolumeToPercent_Test()
        {
            var clinicalProtocolItem = new MaximumClinicalProtocolItem(new UDose(12, UDose.UDoseUnit.Gy),
                new UVolume(5, UVolume.VolumeUnit.ccm), new RtRoiInterpretedType("placeholder", "placeholder"),
                ConstraintType.Hard);
            var expectedClinicalProtocolItem = new MaximumClinicalProtocolItem(new UDose(12, UDose.UDoseUnit.Gy),
                new UVolume(6.25, UVolume.VolumeUnit.percent), new RtRoiInterpretedType("placeholder", "placeholder"),
                ConstraintType.Hard);

            var dvhCurveWithPercent = _dvhCurveWithCcmVolumes.ToVolumeInPercent();

            var (convertedClinicalProtocolItem, convertedDvhCurve) =
                clinicalProtocolItem.EquateVolumesOfProtocolItemAndDvhCurve(dvhCurveWithPercent);

            Assert.AreEqual(expectedClinicalProtocolItem, convertedClinicalProtocolItem);
            Assert.AreEqual(dvhCurveWithPercent, convertedDvhCurve);
        }

        [Test]
        public void EquateVolumesOfProtocolItemAndDvhCurve_ThrowArgumentNullExceptionForNullArgument_Test()
        {
            var clinicalProtocolItem = new MaximumClinicalProtocolItem(new UDose(12, UDose.UDoseUnit.Gy),
                new UVolume(5, UVolume.VolumeUnit.percent), new RtRoiInterpretedType("placeholder", "placeholder"),
                ConstraintType.Hard);

            Assert.Throws<ArgumentNullException>(() =>
                clinicalProtocolItem.EquateVolumesOfProtocolItemAndDvhCurve(null));
        }

        [Test]
        public void
            EquateVolumesOfProtocolItemAndDvhCurve_ThrowUnequalVolumeUnitsException_IfClinicalProtocolItemAndDvhCurvesTotalVolumeUnitsAreNotEqual_Test()
        {
            var clinicalProtocolItem = new MaximumClinicalProtocolItem(new UDose(12, UDose.UDoseUnit.Gy),
                new UVolume(5, UVolume.VolumeUnit.cmm), new RtRoiInterpretedType("placeholder", "placeholder"),
                ConstraintType.Hard);

            var dvhCurveWithPercent = _dvhCurveWithCcmVolumes.ToVolumeInPercent();

            Assert.Throws<UnequalVolumeUnitsException>(() =>
                clinicalProtocolItem.EquateVolumesOfProtocolItemAndDvhCurve(dvhCurveWithPercent));
        }

        [Test]
        public void IsProtocolItemValid_ReturnTrueIfValid_Test()
        {

            var clinicalProtocolItem1 = new MaximumClinicalProtocolItem(new UDose(12, UDose.UDoseUnit.Gy),
                new UVolume(8, UVolume.VolumeUnit.ccm), new RtRoiInterpretedType("placeholder", "placeholder"),
                ConstraintType.Hard);
            var clinicalProtocolItem2 = new MaximumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(70, UVolume.VolumeUnit.ccm), new RtRoiInterpretedType("placeholder", "placeholder"),
                ConstraintType.Hard);

            var expectedResult1 =
                new ClinicalProtocolItemEvaluationResult(true, clinicalProtocolItem1, new UDose(10, UDose.UDoseUnit.Gy));
            var expectedResult2 =
                new ClinicalProtocolItemEvaluationResult(true, clinicalProtocolItem2, new UDose(0, UDose.UDoseUnit.Gy));

            var result1 = clinicalProtocolItem1.IsSatisfiedByDvhCurve(_dvhCurveWithCcmVolumes);
            var result2 = clinicalProtocolItem2.IsSatisfiedByDvhCurve(_secondDvhCurveWithCcmVolumes);

            Assert.AreEqual(expectedResult1, result1);
            Assert.AreEqual(expectedResult2, result2);
        }

        [Test]
        public void IsProtocolItemValid_ReturnFalseIfNotValid_Test()
        {
            var clinicalProtocolItem1 = new MaximumClinicalProtocolItem(new UDose(4, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), new RtRoiInterpretedType("placeholder", "placeholder"),
                ConstraintType.Hard);
            var clinicalProtocolItem2 = new MaximumClinicalProtocolItem(new UDose(2.4, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), new RtRoiInterpretedType("placeholder", "placeholder"),
                ConstraintType.Hard);

            var expectedResult1 =
                new ClinicalProtocolItemEvaluationResult(false, clinicalProtocolItem1, new UDose(5, UDose.UDoseUnit.Gy));
            var expectedResult2 =
                new ClinicalProtocolItemEvaluationResult(false, clinicalProtocolItem2, new UDose(2.5, UDose.UDoseUnit.Gy));

            var result1 = clinicalProtocolItem1.IsSatisfiedByDvhCurve(_dvhCurveWithCcmVolumes);
            var result2 = clinicalProtocolItem2.IsSatisfiedByDvhCurve(_secondDvhCurveWithCcmVolumes);

            Assert.AreEqual(expectedResult1, result1);
            Assert.AreEqual(expectedResult2, result2);
        }


        [Test]
        public void IsProtocolItemValid_ReturnTrue_IfConstraintAsksForVolumeLargerThanStructureVolume_Test()
        {
            var clinicalProtocolItem = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.ccm), new RtRoiInterpretedType("placeholder", "placeholder"),
                ConstraintType.Hard);

            var expectedResult =
                new ClinicalProtocolItemEvaluationResult(true, clinicalProtocolItem, new UDose(0, UDose.UDoseUnit.Gy));

            var result = clinicalProtocolItem.IsSatisfiedByDvhCurve(_dvhCurveWithCcmVolumes);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void ToString_Test()
        {
            var clinicalProtocolItem = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent), new RtRoiInterpretedType("placeholder", "placeholder"),
                ConstraintType.Hard);

            var expectedResult = $"D100% <= 6Gy";
            var result = clinicalProtocolItem.ToString();

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void ToString_FuncAsArgument_Test()
        {
            var clinicalProtocolItem = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(100, UVolume.VolumeUnit.percent), new RtRoiInterpretedType("RT1", "Description of RT 1"),
                ConstraintType.Hard);

            var expectedResult = $"RT1:\nD100% >= 6Gy\nType: Hard";
            var result = clinicalProtocolItem.ToString(ToString);

            Assert.AreEqual(expectedResult, result);
        }

        private string ToString(UVolume volume, UDose dose, ConstraintType type, RtRoiInterpretedType roiType)
        {
            return $"{roiType.Code}:\nD{volume} >= {dose}\nType: {type.ToDescription()}";
        }
    }
}
