﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RTP.CP.Tests
{
    [TestFixture]
    public class MinimumDoseClinicalProtocolItemTests
    {
        private DVHCurve _dvhCurveWithPercentVolumes;
        private DVHCurve _dvhCurveWithCcmVolumes;
        private DVHCurve _secondDvhCurveWithCcmVolumes;
        private DVHCurve _dvhCurveWithMinimumDoseOfFive;

        [SetUp]
        public void SetUp()
        {
            _dvhCurveWithPercentVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(50, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.percent)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL, new UVolume(80, UVolume.VolumeUnit.ccm));

            _dvhCurveWithCcmVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(80, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(40, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(8, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            _secondDvhCurveWithCcmVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(50, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(8, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            _dvhCurveWithMinimumDoseOfFive = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(8, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
        }

        [Test]
        public void IsProtocolItemValid_ReturnTrueIfValid_Test()
        {
            var clinicalProtocolItem =
                new MinimumDoseClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                    new RtRoiInterpretedType("placeholder", "placeholder"),
                    ConstraintType.Hard);

            var expectedResult =
                new ClinicalProtocolItemEvaluationResult(true, clinicalProtocolItem, new UDose(5, UDose.UDoseUnit.Gy));

            var result = clinicalProtocolItem.IsSatisfiedByDvhCurve(_dvhCurveWithMinimumDoseOfFive);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void IsProtocolItemValid_ReturnFalseIfNotValid_Test()
        {

            var clinicalProtocolItem1 =
                new MinimumDoseClinicalProtocolItem(new UDose(5.5, UDose.UDoseUnit.Gy),
                    new RtRoiInterpretedType("placeholder", "placeholder"),
                    ConstraintType.Hard);

            var clinicalProtocolItem2 =
                new MinimumDoseClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                    new RtRoiInterpretedType("placeholder", "placeholder"),
                    ConstraintType.Hard);

            var clinicalProtocolItem3 =
                new MinimumDoseClinicalProtocolItem(new UDose(0.04, UDose.UDoseUnit.Gy),
                    new RtRoiInterpretedType("placeholder", "placeholder"),
                    ConstraintType.Hard);

            var expectedResult =
                new ClinicalProtocolItemEvaluationResult(false, clinicalProtocolItem1,
                    new UDose(5, UDose.UDoseUnit.Gy));
            var expectedResult2 =
                new ClinicalProtocolItemEvaluationResult(false, clinicalProtocolItem2, new UDose(0, UDose.UDoseUnit.Gy));
            var expectedResult3 =
                new ClinicalProtocolItemEvaluationResult(false, clinicalProtocolItem3, new UDose(0, UDose.UDoseUnit.Gy));
            var expectedResult4 =
                new ClinicalProtocolItemEvaluationResult(false, clinicalProtocolItem3, new UDose(0, UDose.UDoseUnit.Gy));

            var result = clinicalProtocolItem1.IsSatisfiedByDvhCurve(_dvhCurveWithMinimumDoseOfFive);
            var result2 = clinicalProtocolItem2.IsSatisfiedByDvhCurve(_secondDvhCurveWithCcmVolumes);
            var result3 = clinicalProtocolItem3.IsSatisfiedByDvhCurve(_dvhCurveWithCcmVolumes);
            var result4 = clinicalProtocolItem3.IsSatisfiedByDvhCurve(_dvhCurveWithPercentVolumes);

            Assert.AreEqual(expectedResult, result);
            Assert.AreEqual(expectedResult2, result2);
            Assert.AreEqual(expectedResult3, result3);
            Assert.AreEqual(expectedResult4, result4);
        }

        [Test]
        public void IsProtocolItemValid_Throw_UnequalDoseUnits_IfDoseUnitsAreNotEqual_Test()
        {
            var minimumDoseClinicalProtocolItem =
                new MinimumDoseClinicalProtocolItem(new UDose(14, UDose.UDoseUnit.cGy),
                    new RtRoiInterpretedType("placeholder", "placeholder"), ConstraintType.Hard);

            Assert.Throws<UnequalDoseUnitsException>(() =>
                minimumDoseClinicalProtocolItem.IsSatisfiedByDvhCurve(_dvhCurveWithCcmVolumes));
            Assert.Throws<UnequalDoseUnitsException>(() =>
                minimumDoseClinicalProtocolItem.IsSatisfiedByDvhCurve(_secondDvhCurveWithCcmVolumes));
            Assert.Throws<UnequalDoseUnitsException>(() =>
                minimumDoseClinicalProtocolItem.IsSatisfiedByDvhCurve(_dvhCurveWithPercentVolumes));
        }


        [Test]
        public void ToString_Test()
        {
            var clinicalProtocolItem =
                new MinimumDoseClinicalProtocolItem(new UDose(14, UDose.UDoseUnit.cGy),
                    new RtRoiInterpretedType("placeholder", "placeholder"), ConstraintType.Hard);

            var expectedResult = $"D_min >= 14cGy";
            var result = clinicalProtocolItem.ToString();

            Assert.AreEqual(expectedResult, result);
        }
    }
}
