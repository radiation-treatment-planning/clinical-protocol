﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RTP.CP.Tests
{
    [TestFixture]
    public class MaximumDoseClinicalProtocolItemTests
    {
        private DVHCurve _dvhCurveWithPercentVolumes;
        private DVHCurve _dvhCurveWithCcmVolumes;
        private DVHCurve _secondDvhCurveWithCcmVolumes;

        [SetUp]
        public void SetUp()
        {
            _dvhCurveWithPercentVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(50, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.percent)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            _dvhCurveWithCcmVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(80, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(40, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(8, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            _secondDvhCurveWithCcmVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(50, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(8, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);
        }

        [Test]
        public void IsProtocolItemValid_ReturnTrueIfValid_Test()
        {

            var maximumDoseClinicalProtocolItem =
                new MaximumDoseClinicalProtocolItem(new UDose(20, UDose.UDoseUnit.Gy),
                    new RtRoiInterpretedType("placeholder", "placeholder"), ConstraintType.Hard);

            var expectedResult1 =
                new ClinicalProtocolItemEvaluationResult(true, maximumDoseClinicalProtocolItem, new UDose(10, UDose.UDoseUnit.Gy));
            var expectedResult2 =
                new ClinicalProtocolItemEvaluationResult(true, maximumDoseClinicalProtocolItem, new UDose(10, UDose.UDoseUnit.Gy));

            var result1 = maximumDoseClinicalProtocolItem.IsSatisfiedByDvhCurve(_dvhCurveWithCcmVolumes);
            var result2 = maximumDoseClinicalProtocolItem.IsSatisfiedByDvhCurve(_secondDvhCurveWithCcmVolumes);

            Assert.AreEqual(expectedResult1, result1);
            Assert.AreEqual(expectedResult2, result2);
        }

        [Test]
        public void IsProtocolItemValid_ReturnFalseIfNotValid_Test()
        {

            var maximumDoseClinicalProtocolItem =
                new MaximumDoseClinicalProtocolItem(new UDose(9, UDose.UDoseUnit.Gy),
                    new RtRoiInterpretedType("placeholder", "placeholder"), ConstraintType.Hard);

            var expectedResult1 =
                new ClinicalProtocolItemEvaluationResult(false, maximumDoseClinicalProtocolItem, new UDose(10, UDose.UDoseUnit.Gy));
            var expectedResult2 =
                new ClinicalProtocolItemEvaluationResult(false, maximumDoseClinicalProtocolItem, new UDose(10, UDose.UDoseUnit.Gy));

            var result1 = maximumDoseClinicalProtocolItem.IsSatisfiedByDvhCurve(_dvhCurveWithCcmVolumes);
            var result2 = maximumDoseClinicalProtocolItem.IsSatisfiedByDvhCurve(_secondDvhCurveWithCcmVolumes);

            Assert.AreEqual(expectedResult1, result1);
            Assert.AreEqual(expectedResult2, result2);
        }

        [Test]
        public void IsProtocolItemValid_Throw_UnequalDoseUnits_IfDoseUnitsAreNotEqual_Test()
        {
            var maximumDoseClinicalProtocolItem =
                new MaximumDoseClinicalProtocolItem(new UDose(14, UDose.UDoseUnit.cGy),
                    new RtRoiInterpretedType("placeholder", "placeholder"), ConstraintType.Hard);

            Assert.Throws<UnequalDoseUnitsException>(() =>
                maximumDoseClinicalProtocolItem.IsSatisfiedByDvhCurve(_dvhCurveWithCcmVolumes));
            Assert.Throws<UnequalDoseUnitsException>(() =>
                maximumDoseClinicalProtocolItem.IsSatisfiedByDvhCurve(_secondDvhCurveWithCcmVolumes));
        }

        [Test]
        public void ToString_Test()
        {
            var clinicalProtocolItem = new MaximumDoseClinicalProtocolItem(new UDose(14, UDose.UDoseUnit.cGy),
                new RtRoiInterpretedType("placeholder", "placeholder"), ConstraintType.Hard);

            var expectedResult = $"D_max <= 14cGy";
            var result = clinicalProtocolItem.ToString();

            Assert.AreEqual(expectedResult, result);
        }
    }
}
