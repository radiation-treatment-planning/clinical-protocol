﻿using System.IO;
using System.Windows.Media.Media3D;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Patient;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RTP.CP.Tests
{
    [TestFixture]
    public class ClinicalProtocolSerializerTests
    {
        private UPatient _patient;

        [SetUp]
        public void SetUp()
        {
            _patient = new UPatient("id", "name");
            var structureSetDetached = new UStructureSetDetached("Study_SIP");
            var structureSet = _patient.AddStructureSet(structureSetDetached);
            structureSet.AddStructure(new UStructureDetached("z_GTV", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.Some(19.7), Option.None<bool>(), Option.None<MeshGeometry3D>()));
            structureSet.AddStructure(new UStructureDetached("z_PTVdom", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.Some(60.4), Option.None<bool>(), Option.None<MeshGeometry3D>()));
            structureSet.AddStructure(new UStructureDetached("z_PTVSIP", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.Some(5.6), Option.None<bool>(), Option.None<MeshGeometry3D>()));
            structureSet.AddStructure(new UStructureDetached("Duodenum", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.Some(826.3), Option.None<bool>(), Option.None<MeshGeometry3D>()));
            structureSet.AddStructure(new UStructureDetached("Kidney Left", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.Some(198.6), Option.None<bool>(), Option.None<MeshGeometry3D>()));
            structureSet.AddStructure(new UStructureDetached("Kidney Right", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.Some(193.2), Option.None<bool>(), Option.None<MeshGeometry3D>()));
            structureSet.AddStructure(new UStructureDetached("Spinal Cord", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.Some(78.8), Option.None<bool>(), Option.None<MeshGeometry3D>())); ;
            structureSet.AddStructure(new UStructureDetached("Stomach", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.Some(132.9), Option.None<bool>(), Option.None<MeshGeometry3D>()));
            structureSet.AddStructure(new UStructureDetached("z_OAR", Option.None<(double, double, double)>(),
                Option.None<string>(), Option.Some(826.3), Option.None<bool>(), Option.None<MeshGeometry3D>()));
        }

        [Test]
        public void Serialize_Test()
        {
            var constraint1 = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);
            var constraint2 = new MinimumClinicalProtocolItem(new UDose(7, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Soft);
            var constraint3 = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Hard);
            var constraint4 = new MaximumClinicalProtocolItem(new UDose(4, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Soft);
            var constraint5 = new MinimumDoseClinicalProtocolItem(new UDose(0.001, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Hard);
            var constraint6 = new MinimumDoseClinicalProtocolItem(new UDose(3, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Soft);
            var constraint7 = new MaximumDoseClinicalProtocolItem(new UDose(16, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Hard);
            var constraint8 = new MinimumDoseClinicalProtocolItem(new UDose(13, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Soft);

            var items = new IClinicalProtocolItem[]
            {
                constraint1, constraint2, constraint3, constraint4, constraint5, constraint6, constraint7,
                constraint8
            };

            var clinicalProtocol = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(items)
                .Build("CP", "Description");

            Assert.DoesNotThrow(() => ClinicalProtocolSerializer.Serialize(clinicalProtocol));
        }

        [Test]
        public void Deserialize_Test()
        {
            var constraint1 = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);
            var constraint2 = new MinimumClinicalProtocolItem(new UDose(7, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Soft);
            var constraint3 = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Hard);
            var constraint4 = new MaximumClinicalProtocolItem(new UDose(4, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Soft);
            var constraint5 = new MinimumDoseClinicalProtocolItem(new UDose(0.001, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Hard);
            var constraint6 = new MinimumDoseClinicalProtocolItem(new UDose(3, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Soft);
            var constraint7 = new MaximumDoseClinicalProtocolItem(new UDose(16, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Hard);
            var constraint8 = new MinimumDoseClinicalProtocolItem(new UDose(13, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Soft);

            var items = new IClinicalProtocolItem[]
            {
                constraint1, constraint2, constraint3, constraint4, constraint5, constraint6, constraint7,
                constraint8
            };

            var clinicalProtocol = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(items)
                .Build("CP", "Description");

            var serializedClinicalPrototol = ClinicalProtocolSerializer.Serialize(clinicalProtocol);
            var result = ClinicalProtocolSerializer.Deserialize(serializedClinicalPrototol);
            Assert.AreEqual(clinicalProtocol, result);
        }

        [Test]
        public void WriteToFile_Test()
        {
            var constraint1 = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);
            var constraint2 = new MinimumClinicalProtocolItem(new UDose(7, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Soft);
            var constraint3 = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Hard);
            var constraint4 = new MaximumClinicalProtocolItem(new UDose(4, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Soft);
            var constraint5 = new MinimumDoseClinicalProtocolItem(new UDose(0.001, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Hard);
            var constraint6 = new MinimumDoseClinicalProtocolItem(new UDose(3, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Soft);
            var constraint7 = new MaximumDoseClinicalProtocolItem(new UDose(16, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Hard);
            var constraint8 = new MinimumDoseClinicalProtocolItem(new UDose(13, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Soft);

            var items = new IClinicalProtocolItem[]
            {
                constraint1, constraint2, constraint3, constraint4, constraint5, constraint6, constraint7,
                constraint8
            };

            var clinicalProtocol = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(items)
                .Build("CP", "Description");

            Assert.IsTrue(ClinicalProtocolSerializer.WriteToFile(new FileInfo("ClinicalProtocolTest.json"),
                clinicalProtocol));
        }

        [Test]
        public void ReadFromFile_Test()
        {
            var constraint1 = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);
            var constraint2 = new MinimumClinicalProtocolItem(new UDose(7, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Soft);
            var constraint3 = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Hard);
            var constraint4 = new MaximumClinicalProtocolItem(new UDose(4, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Soft);
            var constraint5 = new MinimumDoseClinicalProtocolItem(new UDose(0.001, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Hard);
            var constraint6 = new MinimumDoseClinicalProtocolItem(new UDose(3, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Soft);
            var constraint7 = new MaximumDoseClinicalProtocolItem(new UDose(16, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Hard);
            var constraint8 = new MinimumDoseClinicalProtocolItem(new UDose(13, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Soft);

            var items = new IClinicalProtocolItem[]
            {
                constraint1, constraint2, constraint3, constraint4, constraint5, constraint6, constraint7,
                constraint8
            };

            var clinicalProtocol = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(items)
                .Build("CP", "Description");

            var fileInfo = new FileInfo("ClinicalProtocolTest.json");
            var success =
                ClinicalProtocolSerializer.WriteToFile(fileInfo, clinicalProtocol);
            Assert.IsTrue(success);

            var deserializedClinicalProtocol = ClinicalProtocolSerializer.ReadFromFile(fileInfo);
            Assert.AreEqual(clinicalProtocol, deserializedClinicalProtocol);
        }
    }
}
