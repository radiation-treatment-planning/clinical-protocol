﻿using System;
using System.Linq;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RTP.CP.Tests
{
    [TestFixture]
    public class ClinicalProtocolBuilderTests
    {
        [Test]
        public void Build_ThrowArgumentNullExceptionForNullInput_Test()
        {
            Assert.Throws<ArgumentNullException>(() => ClinicalProtocolBuilder.Initialize().Build(null, "Description"));
            Assert.Throws<ArgumentNullException>(() => ClinicalProtocolBuilder.Initialize().Build("CP", null));
        }

        [Test]
        public void Build_WithClinicalProtocolItems_Test()
        {
            var constraint1 = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);
            var constraint2 = new MinimumClinicalProtocolItem(new UDose(7, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Soft);

            var constraint3 = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Hard);
            var constraint4 = new MaximumClinicalProtocolItem(new UDose(4, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Soft);

            var constraint5 = new MinimumDoseClinicalProtocolItem(new UDose(0.001, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Hard);
            var constraint6 = new MinimumDoseClinicalProtocolItem(new UDose(3, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Soft);


            var constraint7 = new MaximumDoseClinicalProtocolItem(new UDose(16, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Hard);
            var constraint8 = new MinimumDoseClinicalProtocolItem(new UDose(13, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Soft);

            var items = new IClinicalProtocolItem[]
            {
                constraint1, constraint2, constraint3, constraint4, constraint5, constraint6, constraint7,
                constraint8
            };

            var result = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(items)
                .Build("CP", "Description");

            Assert.AreEqual("Description", result.Description);
            Assert.AreEqual("CP", result.Id);

            var resultItems = result.ClinicalProtocolItems.ToList();
            Assert.AreEqual(items.Length, resultItems.Count);
            foreach (var clinicalProtocolItem in items) Assert.Contains(clinicalProtocolItem, resultItems);
        }

        [Test]
        public void Build_WithClinicalProtocolItem_Test()
        {
            var constraint1 = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);
            var constraint2 = new MinimumClinicalProtocolItem(new UDose(7, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Soft);

            var constraint3 = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Hard);
            var constraint4 = new MaximumClinicalProtocolItem(new UDose(4, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Soft);

            var constraint5 = new MinimumDoseClinicalProtocolItem(new UDose(0.001, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Hard);
            var constraint6 = new MinimumDoseClinicalProtocolItem(new UDose(3, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Soft);


            var constraint7 = new MaximumDoseClinicalProtocolItem(new UDose(16, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Hard);
            var constraint8 = new MinimumDoseClinicalProtocolItem(new UDose(13, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Soft);

            var items = new IClinicalProtocolItem[]
            {
                constraint1, constraint2, constraint3, constraint4, constraint5, constraint6, constraint7,
                constraint8
            };

            var result = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItem(constraint1)
                .WithClinicalProtocolItem(constraint2)
                .WithClinicalProtocolItem(constraint3)
                .WithClinicalProtocolItem(constraint4)
                .WithClinicalProtocolItem(constraint5)
                .WithClinicalProtocolItem(constraint6)
                .WithClinicalProtocolItem(constraint7)
                .WithClinicalProtocolItem(constraint8)
                .Build("CP", "Description");

            Assert.AreEqual("Description", result.Description);
            Assert.AreEqual("CP", result.Id);

            var resultItems = result.ClinicalProtocolItems.ToList();
            Assert.AreEqual(items.Length, resultItems.Count);
            foreach (var clinicalProtocolItem in items) Assert.Contains(clinicalProtocolItem, resultItems);
        }
    }
}
