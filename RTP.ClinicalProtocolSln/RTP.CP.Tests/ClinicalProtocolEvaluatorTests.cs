﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RTP.CP.Tests
{
    [TestFixture]
    public class ClinicalProtocolEvaluatorTests
    {
        private DVHCurve _dvhCurveWithPercentVolumes;
        private ClinicalProtocolEvaluator _clinicalProtocolEvaluator;
        private DVHCurve _dvhCurveWithCcmVolumes;
        private DVHCurve _secondDvhCurveWithCcmVolumes;

        [SetUp]
        public void SetUp()
        {
            _clinicalProtocolEvaluator = new ClinicalProtocolEvaluator();

            _dvhCurveWithPercentVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(100, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(50, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(10, UVolume.VolumeUnit.percent)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.percent)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL, new UVolume(80, UVolume.VolumeUnit.ccm));

            _dvhCurveWithCcmVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(80, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(40, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(8, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);

            _secondDvhCurveWithCcmVolumes = new DVHCurve(new List<Tuple<UDose, UVolume>>
            {
                Tuple.Create(new UDose(0, UDose.UDoseUnit.Gy), new UVolume(50, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(5, UDose.UDoseUnit.Gy), new UVolume(30, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(10, UDose.UDoseUnit.Gy), new UVolume(8, UVolume.VolumeUnit.ccm)),
                Tuple.Create(new UDose(15, UDose.UDoseUnit.Gy), new UVolume(0, UVolume.VolumeUnit.ccm)),
            }, DVHCurve.Type.CUMULATIVE, DVHCurve.DoseType.PHYSICAL);


        }

        [Test]
        public void Evaluate_SomeConstraintsNotSatisfied_Test()
        {
            var constraint1 = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);
            var constraint2 = new MinimumClinicalProtocolItem(new UDose(7, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Soft);

            var constraint3 = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Hard);
            var constraint4 = new MaximumClinicalProtocolItem(new UDose(4, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Soft);

            var constraint5 = new MinimumDoseClinicalProtocolItem(new UDose(0.001, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Hard);
            var constraint6 = new MinimumDoseClinicalProtocolItem(new UDose(0.05, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Hard);


            var constraint7 = new MaximumDoseClinicalProtocolItem(new UDose(16, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Hard);
            var constraint8 = new MinimumDoseClinicalProtocolItem(new UDose(13, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Soft);

            var clinicalProtocol = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(new IClinicalProtocolItem[]
                {
                    constraint1, constraint2, constraint3, constraint4, constraint5, constraint6, constraint7,
                    constraint8
                })
                .Build("CP", "Description");

            var config = new DVHCurveAndROITypeConfig(new[]
            {
                new DVHCurveAndROITypeTuple(_dvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.GTV()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.GTV()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.LEFT_KIDNEY()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.LEFT_KIDNEY()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.PTV()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.PTV()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.SMALL_BOWEL()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.SMALL_BOWEL()),
            });

            var expectedClinicalProtocolItemEvaluationResults = new List<ClinicalProtocolItemEvaluationResult>
            {
                new ClinicalProtocolItemEvaluationResult(true, constraint1, new UDose(5, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(false, constraint2, new UDose(5, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(true, constraint3, new UDose(5, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(false, constraint4, new UDose(5, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(false, constraint5, new UDose(0, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(false, constraint6, new UDose(0, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(true, constraint7, new UDose(10, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(false, constraint8, new UDose(0, UDose.UDoseUnit.Gy)),
            };


            var expectedResult = new ClinicalProtocolEvaluationResult(expectedClinicalProtocolItemEvaluationResults);
            var result = _clinicalProtocolEvaluator.Evaluate(clinicalProtocol, config);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void Evaluate_SoftConstraintsAreNotValid_ButHardConstraintsAre_Test()
        {
            var constraint1 = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);
            var constraint2 = new MinimumClinicalProtocolItem(new UDose(7, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.AVOIDANCE(),
                ConstraintType.Soft);

            var constraint3 = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Hard);
            var constraint4 = new MaximumClinicalProtocolItem(new UDose(4, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.RIGHT_KIDNEY(),
                ConstraintType.Soft);

            var constraint5 = new MinimumDoseClinicalProtocolItem(new UDose(0, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Hard);
            var constraint6 = new MinimumDoseClinicalProtocolItem(new UDose(3, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV_DOM(), ConstraintType.Soft);


            var constraint7 = new MaximumDoseClinicalProtocolItem(new UDose(16, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Hard);
            var constraint8 = new MinimumDoseClinicalProtocolItem(new UDose(13, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Soft);

            var clinicalProtocol = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(new IClinicalProtocolItem[]
                {
                    constraint1, constraint2, constraint3, constraint4, constraint5, constraint6, constraint7,
                    constraint8
                })
                .Build("CP", "Description");

            var config = new DVHCurveAndROITypeConfig(new[]
            {
                new DVHCurveAndROITypeTuple(_dvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.GTV()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.AVOIDANCE()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.LEFT_KIDNEY()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.RIGHT_KIDNEY()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.PTV()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.PTV_DOM()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.SMALL_BOWEL()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.SMALL_BOWEL()),
            });

            var expectedClinicalProtocolItemEvaluationResults = new List<ClinicalProtocolItemEvaluationResult>
            {
                new ClinicalProtocolItemEvaluationResult(true, constraint1, new UDose(5, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(false, constraint2, new UDose(5, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(true, constraint3, new UDose(5, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(false, constraint4, new UDose(5, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(true, constraint5, new UDose(0, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(false, constraint6, new UDose(0, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(true, constraint7, new UDose(10, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(false, constraint8, new UDose(0, UDose.UDoseUnit.Gy)),
            };

            var expectedResult = new ClinicalProtocolEvaluationResult(expectedClinicalProtocolItemEvaluationResults);
            var result = _clinicalProtocolEvaluator.Evaluate(clinicalProtocol, config);

            for (int i = 0; i < expectedClinicalProtocolItemEvaluationResults.Count; i++)
            {
                var er1 = expectedClinicalProtocolItemEvaluationResults.ElementAt(i);
                var r1 = result.ClinicalProtocolItemEvaluationResults.ElementAt(i);
                Assert.AreEqual(er1, r1);
            }
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void Evaluate_AllConstraintsSatisfied_Test()
        {
            var constraint1 = new MinimumClinicalProtocolItem(new UDose(5, UDose.UDoseUnit.Gy),
                new UVolume(50, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                ConstraintType.Hard);
            var constraint2 = new MaximumClinicalProtocolItem(new UDose(6, UDose.UDoseUnit.Gy),
                new UVolume(40, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                ConstraintType.Hard);
            var constraint3 = new MinimumDoseClinicalProtocolItem(new UDose(0, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.PTV(), ConstraintType.Hard);
            var constraint4 = new MaximumDoseClinicalProtocolItem(new UDose(16, UDose.UDoseUnit.Gy),
                RtRoiInterpretedTypePresets.SMALL_BOWEL(), ConstraintType.Hard);

            var expectedClinicalProtocolItemEvaluationResults = new List<ClinicalProtocolItemEvaluationResult>
            {
                new ClinicalProtocolItemEvaluationResult(true, constraint1, new UDose(5, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(true, constraint2, new UDose(5, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(true, constraint3, new UDose(0, UDose.UDoseUnit.Gy)),
                new ClinicalProtocolItemEvaluationResult(true, constraint4, new UDose(10, UDose.UDoseUnit.Gy)),
            };

            var clinicalProtocol = ClinicalProtocolBuilder.Initialize()
                .WithClinicalProtocolItems(new IClinicalProtocolItem[]
                {
                    constraint1, constraint2, constraint3, constraint4
                })
                .Build("CP", "Description");

            var config = new DVHCurveAndROITypeConfig(new[]
            {
                new DVHCurveAndROITypeTuple(_dvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.GTV()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.GTV()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.LEFT_KIDNEY()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.LEFT_KIDNEY()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.PTV()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.PTV()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithCcmVolumes, RtRoiInterpretedTypePresets.SMALL_BOWEL()),
                new DVHCurveAndROITypeTuple(_dvhCurveWithPercentVolumes, RtRoiInterpretedTypePresets.SMALL_BOWEL()),
            });

            var expectedResult = new ClinicalProtocolEvaluationResult(expectedClinicalProtocolItemEvaluationResults);
            var result = _clinicalProtocolEvaluator.Evaluate(clinicalProtocol, config);

            Assert.AreEqual(expectedResult, result);
        }
    }
}
