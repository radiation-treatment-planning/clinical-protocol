﻿using System;
using System.Collections.Generic;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RTP.CP
{
    public class ClinicalProtocolBuilder
    {

        private List<IClinicalProtocolItem> _clinicalProtocolItems;

        internal ClinicalProtocolBuilder()
        {
            _clinicalProtocolItems = new List<IClinicalProtocolItem>();
        }

        public static ClinicalProtocolBuilder Initialize() => new ClinicalProtocolBuilder();

        public ClinicalProtocolBuilder WithClinicalProtocolItem(IClinicalProtocolItem clinicalProtocolItem)
        {
            if (clinicalProtocolItem == null) throw new ArgumentNullException(nameof(clinicalProtocolItem));
            _clinicalProtocolItems.Add(clinicalProtocolItem);
            return this;
        }
        public ClinicalProtocolBuilder WithClinicalProtocolItems(IEnumerable<IClinicalProtocolItem> clinicalProtocolItems)
        {
            if (clinicalProtocolItems == null) throw new ArgumentNullException(nameof(clinicalProtocolItems));
            _clinicalProtocolItems.AddRange(clinicalProtocolItems);
            return this;
        }

        public ClinicalProtocol Build(string clinicalProtocolId, string clinicalProtocolDescription)
        {
            if (clinicalProtocolId == null) throw new ArgumentNullException(nameof(clinicalProtocolId));
            if (clinicalProtocolDescription == null)
                throw new ArgumentNullException(nameof(clinicalProtocolDescription));
            return new ClinicalProtocol(clinicalProtocolId, _clinicalProtocolItems, clinicalProtocolDescription);
        }

        public ClinicalProtocol BuildPancreasClinicalProtocol()
        {
            var clinicalProtocolItems = new IClinicalProtocolItem[]
            {
                new MinimumClinicalProtocolItem(new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                    ConstraintType.Soft),
                new MinimumClinicalProtocolItem(new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.PTV_DOM(),
                    ConstraintType.Soft),
                new MaximumClinicalProtocolItem(new UDose(79.2, UDose.UDoseUnit.Gy),
                    new UVolume(2, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.PTV_DOM(),
                    ConstraintType.Hard),
                new MinimumClinicalProtocolItem(new UDose(72.6, UDose.UDoseUnit.Gy),
                    new UVolume(2, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.PTV_DOM(),
                    ConstraintType.Soft),

                new MaximumClinicalProtocolItem(new UDose(47.4, UDose.UDoseUnit.Gy),
                    new UVolume(2, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(45, UDose.UDoseUnit.Gy),
                    new UVolume(0.5, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(44.4, UDose.UDoseUnit.Gy),
                    new UVolume(5, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(43.2, UDose.UDoseUnit.Gy),
                    new UVolume(10, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(42.0, UDose.UDoseUnit.Gy),
                    new UVolume(15, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard),


                new MaximumClinicalProtocolItem(new UDose(47.4, UDose.UDoseUnit.Gy),
                    new UVolume(2, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.STOMACH(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(45, UDose.UDoseUnit.Gy),
                    new UVolume(0.5, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.STOMACH(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(44.4, UDose.UDoseUnit.Gy),
                    new UVolume(5, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.STOMACH(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(43.2, UDose.UDoseUnit.Gy),
                    new UVolume(10, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.STOMACH(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(42.0, UDose.UDoseUnit.Gy),
                    new UVolume(15, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.STOMACH(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(37.2, UDose.UDoseUnit.Gy),

                    new UVolume(0.5, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SPINAL_CORD(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(24, UDose.UDoseUnit.Gy),
                    new UVolume(0.1, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.LEFT_KIDNEY(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(24, UDose.UDoseUnit.Gy),
                    new UVolume(0.1, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.RIGHT_KIDNEY(),
                    ConstraintType.Hard),

                new MaximumClinicalProtocolItem(new UDose(28.2, UDose.UDoseUnit.Gy),
                    new UVolume(2, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.LIVER(),
                    ConstraintType.Hard),
            };

            return new ClinicalProtocol("Pancreas", clinicalProtocolItems,
                "Pancreas clinical protocol of Auto planning study 2022/23 by Dejan Kostyszyn");
        }

        public ClinicalProtocol BuildPancreasClinicalProtocolLite()
        {
            var clinicalProtocolItems = new IClinicalProtocolItem[]
            {
                new MinimumClinicalProtocolItem(new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                    ConstraintType.Soft),
                new MinimumClinicalProtocolItem(new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.PTV_DOM(),
                    ConstraintType.Soft),
                new MaximumClinicalProtocolItem(new UDose(79.2, UDose.UDoseUnit.Gy),
                    new UVolume(2, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.PTV_DOM(),
                    ConstraintType.Hard),
                new MinimumClinicalProtocolItem(new UDose(72.6, UDose.UDoseUnit.Gy),
                    new UVolume(2, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.PTV_DOM(),
                    ConstraintType.Soft),

                new MaximumClinicalProtocolItem(new UDose(45, UDose.UDoseUnit.Gy),
                    new UVolume(0.5, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(44.4, UDose.UDoseUnit.Gy),
                    new UVolume(5, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(43.2, UDose.UDoseUnit.Gy),
                    new UVolume(10, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(42.0, UDose.UDoseUnit.Gy),
                    new UVolume(15, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard)
            };

            return new ClinicalProtocol("Pancreas Lite", clinicalProtocolItems,
                "Lite Pancreas clinical protocol of Auto planning study 2022/23 by Dejan Kostyszyn");
        }


        public ClinicalProtocol BuildPancreasGtvPtvdomStomachSmallBowel()
        {
            var clinicalProtocolItems = new IClinicalProtocolItem[]
            {
                new MinimumClinicalProtocolItem(new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.GTV(),
                    ConstraintType.Soft),
                new MinimumClinicalProtocolItem(new UDose(66, UDose.UDoseUnit.Gy),
                    new UVolume(98, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.PTV_DOM(),
                    ConstraintType.Soft),
                new MaximumClinicalProtocolItem(new UDose(79.2, UDose.UDoseUnit.Gy),
                    new UVolume(2, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.PTV_DOM(),
                    ConstraintType.Hard),
                new MinimumClinicalProtocolItem(new UDose(72.6, UDose.UDoseUnit.Gy),
                    new UVolume(2, UVolume.VolumeUnit.percent), RtRoiInterpretedTypePresets.PTV_DOM(),
                    ConstraintType.Soft),

                new MaximumClinicalProtocolItem(new UDose(45, UDose.UDoseUnit.Gy),
                    new UVolume(0.5, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(44.4, UDose.UDoseUnit.Gy),
                    new UVolume(5, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(43.2, UDose.UDoseUnit.Gy),
                    new UVolume(10, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(42.0, UDose.UDoseUnit.Gy),
                    new UVolume(15, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.SMALL_BOWEL(),
                    ConstraintType.Hard),

                new MaximumClinicalProtocolItem(new UDose(45, UDose.UDoseUnit.Gy),
                    new UVolume(0.5, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.STOMACH(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(44.4, UDose.UDoseUnit.Gy),
                    new UVolume(5, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.STOMACH(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(43.2, UDose.UDoseUnit.Gy),
                    new UVolume(10, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.STOMACH(),
                    ConstraintType.Hard),
                new MaximumClinicalProtocolItem(new UDose(42.0, UDose.UDoseUnit.Gy),
                    new UVolume(15, UVolume.VolumeUnit.ccm), RtRoiInterpretedTypePresets.STOMACH(),
                    ConstraintType.Hard)
            };

            return new ClinicalProtocol("Pancreas GTV PTVdom Stomach SmallBowel", clinicalProtocolItems,
                "Pancreas clinical protocol of Auto planning study 2022/23 by Dejan Kostyszyn," +
                "with only GTV, PTVdom, Stomach and Small Bowel constraints.");
        }
    }
}
