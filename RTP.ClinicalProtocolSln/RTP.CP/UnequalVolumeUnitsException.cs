﻿using System;

namespace RTP.CP
{
    public class UnequalVolumeUnitsException : Exception
    {
        public UnequalVolumeUnitsException(string message) : base(message)
        {

        }

        public UnequalVolumeUnitsException()
        {

        }
    }
}
