﻿using System;

namespace RTP.CP
{
    public class RelativeDoseDeviation
    {
        public double Value { get; }

        /// <summary>
        /// The relative absolute dose deviation in percent.
        /// </summary>
        /// <param name="value">The relative absolute dose deviation in percent (value > 0).</param>
        public RelativeDoseDeviation(double value)
        {
            if (value < 0) throw new ArgumentOutOfRangeException(nameof(value));
            Value = value;
        }

        public override bool Equals(object obj) => Equals(obj as RelativeDoseDeviation);

        public bool Equals(RelativeDoseDeviation other)
        {
            if (other == null) return false;
            return Math.Abs(Value - other.Value) < 0.001;
        }

        public override int GetHashCode() => Value.GetHashCode();
    }
}
