﻿using System;

namespace RTP.CP
{
    public class UnequalDoseUnitsException : Exception
    {
        public UnequalDoseUnitsException(string message) : base(message)
        {

        }

        public UnequalDoseUnitsException()
        {

        }
    }
}
