﻿using System;

namespace RTP.CP
{
    public class NoDvhCurveForClinicalProtocolItemDefinedException : Exception
    {
        public NoDvhCurveForClinicalProtocolItemDefinedException()
        {

        }

        public NoDvhCurveForClinicalProtocolItemDefinedException(string message) : base(message)
        {

        }
    }
}
