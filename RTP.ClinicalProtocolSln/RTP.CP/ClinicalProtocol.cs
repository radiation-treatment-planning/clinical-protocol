﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RTP.CP
{
    public class ClinicalProtocol
    {
        public string Id { get; }
        public string Description { get; }
        public IEnumerable<IClinicalProtocolItem> ClinicalProtocolItems { get; }

        /// <summary>
        /// A clinical protocol that defines dose constraints for radiation
        /// treatment plans.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="clinicalProtocolItems"></param>
        /// <param name="description"></param>
        public ClinicalProtocol(string id, IEnumerable<IClinicalProtocolItem> clinicalProtocolItems,
            string description)
        {
            Id = id ?? throw new ArgumentNullException(nameof(id));
            Description = description ?? throw new ArgumentNullException(nameof(description));
            ClinicalProtocolItems =
                clinicalProtocolItems ?? throw new ArgumentNullException(nameof(clinicalProtocolItems));
            ThrowArgumentExceptionIfClinicalProtocolItemsHaveDuplicates();
        }

        private void ThrowArgumentExceptionIfClinicalProtocolItemsHaveDuplicates()
        {
            if (!ClinicalProtocolItems.All(new HashSet<IClinicalProtocolItem>().Add))
                throw new DuplicateClinicalProtocolItemException();
        }

        protected bool Equals(ClinicalProtocol other)
        {
            if (ClinicalProtocolItems.Count() != other.ClinicalProtocolItems.Count()) return false;
            foreach (var item in ClinicalProtocolItems)
                if (!other.ClinicalProtocolItems.Contains(item))
                    return false;
            return Id == other.Id &&
                   Description == other.Description;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ClinicalProtocol)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ClinicalProtocolItems, Id, Description);
        }
    }
}
