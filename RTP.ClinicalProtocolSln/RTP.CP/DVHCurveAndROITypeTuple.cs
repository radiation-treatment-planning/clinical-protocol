﻿using System;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.ROIType;

namespace RTP.CP
{
    public class DVHCurveAndROITypeTuple
    {
        public DVHCurve DvhCurve { get; }
        public RtRoiInterpretedType RoiInterpretedType { get; }

        /// <summary>
        /// A tuple that contains a DVHCurve and it's according
        /// RT ROI interpreted type.
        /// </summary>
        /// <param name="dvhCurve">Dose volume histogram curve.</param>
        /// <param name="roiInterpretedType">Ratiation treatment region of interest interpreted type.</param>
        public DVHCurveAndROITypeTuple(DVHCurve dvhCurve, RtRoiInterpretedType roiInterpretedType)
        {
            DvhCurve = dvhCurve ?? throw new ArgumentNullException(nameof(dvhCurve));
            RoiInterpretedType = roiInterpretedType ?? throw new ArgumentNullException(nameof(roiInterpretedType));
        }

        protected bool Equals(DVHCurveAndROITypeTuple other)
        {
            return Equals(DvhCurve, other.DvhCurve) && Equals(RoiInterpretedType, other.RoiInterpretedType);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((DVHCurveAndROITypeTuple)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(DvhCurve, RoiInterpretedType);
        }
    }
}
