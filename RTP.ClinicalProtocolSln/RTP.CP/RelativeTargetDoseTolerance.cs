﻿using System;

namespace RTP.CP
{
    public class RelativeTargetDoseTolerance
    {
        public double Value { get; }
        /// <summary>
        /// The relative tolerance between the target dose of a clinical protocol item
        /// and the actually measured dose.
        /// </summary>
        /// <param name="value">A value in percent [0, 100].</param>
        public RelativeTargetDoseTolerance(double value)
        {
            if (value < 0 || value > 100)
                throw new ArgumentOutOfRangeException($"{nameof(value)} must be >= 0 and <= 100, but was {value}.");
            Value = value;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as RelativeTargetDoseTolerance);
        }

        public bool Equals(RelativeTargetDoseTolerance other)
        {
            if (other == null) return false;
            return Math.Abs(Value - other.Value) < 0.001;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
