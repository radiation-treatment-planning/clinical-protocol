﻿using System;
using System.ComponentModel;
using RadiationTreatmentPlanner.Utils;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RTP.CP
{
    public class MinimumClinicalProtocolItem : IClinicalProtocolItem
    {
        public ConstraintType ConstraintType { get; }
        public UDose Dose { get; }
        public RtRoiInterpretedType RegionOfInterestType { get; }
        public UVolume Volume { get; }

        /// <summary>
        /// Require structure with ROI type <paramref name="regionOfInterestType"/> to have minimum 
        /// dose <paramref name="dose"/> at volume <paramref name="volume"/>.
        /// </summary>
        /// <param name="dose">Target dose.</param>
        /// <param name="volume">Target volume.</param>
        /// <param name="regionOfInterestType">RT ROI Interpreted Type.</param>
        /// <param name="constraintType">Type of constraint.</param>
        public MinimumClinicalProtocolItem(UDose dose, UVolume volume, RtRoiInterpretedType regionOfInterestType,
            ConstraintType constraintType)
        {
            if (!Enum.IsDefined(typeof(ConstraintType), constraintType))
                throw new InvalidEnumArgumentException(nameof(constraintType), (int)constraintType,
                    typeof(ConstraintType));
            Dose = dose ?? throw new ArgumentNullException(nameof(dose));
            Volume = volume ?? throw new ArgumentNullException(nameof(volume));
            RegionOfInterestType =
                regionOfInterestType ?? throw new ArgumentNullException(nameof(regionOfInterestType));
            ConstraintType = constraintType;
        }

        public IClinicalProtocolItem ToVolumeInPercent(UVolume totalStructureVolume)
        {
            if (Volume.Unit == UVolume.VolumeUnit.percent) return this;
            if (totalStructureVolume.Unit != Volume.Unit)
                throw new NotImplementedException(
                    $"Method is only implemented for volumes of equal unit, but received {totalStructureVolume.Unit} and {Volume.Unit}");
            var volumeInPercent = new UVolume(Volume.Value / totalStructureVolume.Value * 100, UVolume.VolumeUnit.percent);
            return new MinimumClinicalProtocolItem(Dose, volumeInPercent, RegionOfInterestType, ConstraintType);
        }

        public ClinicalProtocolItemEvaluationResult IsSatisfiedByDvhCurve(DVHCurve dvhCurve)
        {
            if (dvhCurve == null) throw new ArgumentNullException(nameof(dvhCurve));
            if (dvhCurve.DVHType != DVHCurve.Type.CUMULATIVE)
                throw new NotImplementedException(
                    $"Only implemented for DVH curves of type " +
                    $"{DVHCurve.Type.CUMULATIVE.ToDescription()}, but was " +
                    $"{dvhCurve.DVHType.ToDescription()}.");

            var structuresMaximumVolume = dvhCurve.GetMaximumVolume();
            if (Volume.Unit != structuresMaximumVolume.Unit)
                throw new UnequalVolumeUnitsException(
                    $"Volume units of clinical protocol item and DVH curve must be " +
                    $"equal, but were {Volume.Unit} and {structuresMaximumVolume.Unit}.");

            var actualDose = dvhCurve.GetDoseAtVolume(Volume);
            if (Dose.Unit != actualDose.Unit) throw new UnequalDoseUnitsException(
                $"Dose units of clinical protocol item and DVH curve must be " +
                $"equal, but were {Dose.Unit} and {actualDose.Unit}.");

            var isClinicalProtocolItemSatisfied = !(Dose.Value > actualDose.Value);
            return new ClinicalProtocolItemEvaluationResult(isClinicalProtocolItemSatisfied, this, actualDose);
        }

        public (IClinicalProtocolItem, DVHCurve) EquateVolumesOfProtocolItemAndDvhCurve(DVHCurve dvhCurve)
        {
            if (dvhCurve == null) throw new ArgumentNullException(nameof(dvhCurve));
            var targetVolumeUnit = Volume.Unit;
            var dvhCurveVolumeUnit = dvhCurve.GetVolumeUnit();

            // If volume units are same, return as they are.
            if (targetVolumeUnit == dvhCurveVolumeUnit) return (this, dvhCurve);

            // Convert DVHCurve to percent case.
            if (targetVolumeUnit == UVolume.VolumeUnit.percent && dvhCurveVolumeUnit != UVolume.VolumeUnit.percent)
                return (this, dvhCurve.ToVolumeInPercent());

            // Convert clinical protocol item to percent case.
            if (targetVolumeUnit != UVolume.VolumeUnit.percent && dvhCurveVolumeUnit == UVolume.VolumeUnit.percent)
            {
                var totalStructureVolume = dvhCurve.TotalVolume();
                if (targetVolumeUnit != totalStructureVolume.Unit)
                    throw new UnequalVolumeUnitsException(
                        $"To convert clinical protocol item's volume unit to percent, the item's volume " +
                        $"and DVHCurve's total volume units must be equal, but were {targetVolumeUnit} " +
                        $"and {totalStructureVolume.Unit}.");
                var clinicalProtocolVolumeInPercent =
                    ((MinimumClinicalProtocolItem)ToVolumeInPercent(totalStructureVolume)).Volume;
                return (new MinimumClinicalProtocolItem(Dose, clinicalProtocolVolumeInPercent,
                    RegionOfInterestType, ConstraintType), dvhCurve);
            }

            throw new ArgumentException(
                $"Neither clinical protocol item's volume could be fit to DVHCurves volume, nor vice versa.");
        }

        protected bool Equals(MinimumClinicalProtocolItem other)
        {
            return ConstraintType == other.ConstraintType && Equals(Dose, other.Dose) &&
                   Equals(RegionOfInterestType, other.RegionOfInterestType) && Equals(Volume, other.Volume);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((MinimumClinicalProtocolItem)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine((int)ConstraintType, Dose, RegionOfInterestType, Volume);
        }

        public override string ToString()
        {
            return $"D{Volume} >= {Dose}";
        }
        public string ToString(Func<UVolume, UDose, ConstraintType, RtRoiInterpretedType, string> toString)
        {
            return toString(Volume, Dose, ConstraintType, RegionOfInterestType);
        }
    }
}
