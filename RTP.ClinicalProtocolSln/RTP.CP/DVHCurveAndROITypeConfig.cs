﻿using System;
using System.Collections.Generic;

namespace RTP.CP
{
    public class DVHCurveAndROITypeConfig
    {
        public IEnumerable<DVHCurveAndROITypeTuple> DvhCurveAndRoiTypeTuples { get; }

        /// <summary>
        /// Configuration of DVH Curve and RT ROI Interpreted type tuples.
        /// </summary>
        /// <param name="dvhCurveAndRoiTypeTuples">DVH Curve and RT ROI Interpreted type tuples.</param>
        public DVHCurveAndROITypeConfig(IEnumerable<DVHCurveAndROITypeTuple> dvhCurveAndRoiTypeTuples)
        {
            DvhCurveAndRoiTypeTuples = dvhCurveAndRoiTypeTuples ??
                                       throw new ArgumentNullException(nameof(dvhCurveAndRoiTypeTuples));
        }
    }
}
