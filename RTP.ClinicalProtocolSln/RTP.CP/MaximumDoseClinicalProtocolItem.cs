﻿using System;
using System.ComponentModel;
using RadiationTreatmentPlanner.Utils;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RTP.CP
{
    public class MaximumDoseClinicalProtocolItem : IClinicalProtocolItem
    {
        public ConstraintType ConstraintType { get; }
        public UDose Dose { get; }
        public RtRoiInterpretedType RegionOfInterestType { get; }

        /// <summary>
        /// Require structure with ROI type <paramref name="regionOfInterestType"/> to have
        /// maximum dose <paramref name="dose"/> at all volumes.
        /// </summary>
        /// <param name="dose">Target dose.</param>
        /// <param name="regionOfInterestType">RT ROI Interpreted Type.</param>
        /// <param name="constraintType">Type of constraint.</param>
        public MaximumDoseClinicalProtocolItem(UDose dose, RtRoiInterpretedType regionOfInterestType,
            ConstraintType constraintType)
        {
            if (!Enum.IsDefined(typeof(ConstraintType), constraintType))
                throw new InvalidEnumArgumentException(nameof(constraintType), (int)constraintType,
                    typeof(ConstraintType));
            Dose = dose ?? throw new ArgumentNullException(nameof(dose));
            RegionOfInterestType =
                regionOfInterestType ?? throw new ArgumentNullException(nameof(regionOfInterestType));
            ConstraintType = constraintType;
        }

        public IClinicalProtocolItem ToVolumeInPercent(UVolume totalStructureVolume) => this;
        public ClinicalProtocolItemEvaluationResult IsSatisfiedByDvhCurve(DVHCurve dvhCurve)
        {
            if (dvhCurve == null) throw new ArgumentNullException(nameof(dvhCurve));
            if (dvhCurve.DVHType != DVHCurve.Type.CUMULATIVE)
                throw new NotImplementedException(
                    $"Only implemented for DVH curves of type " +
                    $"{DVHCurve.Type.CUMULATIVE.ToDescription()}, but was " +
                    $"{dvhCurve.DVHType.ToDescription()}.");
            var dvhCurveMaximumDose = dvhCurve.GetMaximumDose();

            if (Dose.Unit != dvhCurveMaximumDose.Unit)
                throw new UnequalDoseUnitsException(
                    $"Dose units of clinical protocol item and DVH curve must be " +
                    $"equal, but were {Dose.Unit} and {dvhCurveMaximumDose.Unit}.");

            var isClinicalProtocolItemSatisfied = !(Dose.Value < dvhCurveMaximumDose.Value);
            return new ClinicalProtocolItemEvaluationResult(isClinicalProtocolItemSatisfied, this, dvhCurveMaximumDose);
        }

        public (IClinicalProtocolItem, DVHCurve) EquateVolumesOfProtocolItemAndDvhCurve(DVHCurve dvhCurve)
        {
            return (this, dvhCurve);
        }

        protected bool Equals(MaximumDoseClinicalProtocolItem other)
        {
            return ConstraintType == other.ConstraintType && Equals(Dose, other.Dose) &&
                   Equals(RegionOfInterestType, other.RegionOfInterestType);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((MaximumDoseClinicalProtocolItem)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine((int)ConstraintType, Dose, RegionOfInterestType);
        }

        public override string ToString()
        {
            return $"D_max <= {Dose}";
        }

        public string ToString(Func<UVolume, UDose, ConstraintType, RtRoiInterpretedType, string> toString)
        {
            return toString(new UVolume(double.NaN, UVolume.VolumeUnit.Undefined), Dose, ConstraintType, RegionOfInterestType);
        }
    }
}
