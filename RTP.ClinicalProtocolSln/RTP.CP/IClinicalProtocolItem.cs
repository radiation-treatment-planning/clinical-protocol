﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.ROIType;
using RadiationTreatmentPlanner.Utils.Volume;

namespace RTP.CP
{
    public interface IClinicalProtocolItem
    {
        ConstraintType ConstraintType { get; }
        UDose Dose { get; }
        RtRoiInterpretedType RegionOfInterestType { get; }

        IClinicalProtocolItem ToVolumeInPercent(UVolume totalStructureVolume);
        ClinicalProtocolItemEvaluationResult IsSatisfiedByDvhCurve(DVHCurve dvhCurve);
        (IClinicalProtocolItem, DVHCurve) EquateVolumesOfProtocolItemAndDvhCurve(DVHCurve dvhCurve);
        string ToString(Func<UVolume, UDose, ConstraintType, RtRoiInterpretedType, string> toString);
    }
}
