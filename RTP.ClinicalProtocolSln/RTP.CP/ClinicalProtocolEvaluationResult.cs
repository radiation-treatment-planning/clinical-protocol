﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RTP.CP
{
    public class ClinicalProtocolEvaluationResult
    {
        public bool AreAllConstraintsSatisfied { get; }
        public bool AreAllHardConstraintsSatisfied { get; }
        public bool AreAllSoftConstraintsSatisfied { get; }
        public IEnumerable<ClinicalProtocolItemEvaluationResult> ClinicalProtocolItemEvaluationResults { get; }

        public ClinicalProtocolEvaluationResult(IEnumerable<ClinicalProtocolItemEvaluationResult> clinicalProtocolItemEvaluationResults)
        {
            ClinicalProtocolItemEvaluationResults = clinicalProtocolItemEvaluationResults ??
                                                    throw new ArgumentNullException(
                                                        nameof(clinicalProtocolItemEvaluationResults));
            AreAllHardConstraintsSatisfied =
                ClinicalProtocolItemEvaluationResults
                    .Where(x => x.ClinicalProtocolItem.ConstraintType == ConstraintType.Hard)
                    .All(y => y.IsClinicalProtocolSatisfied);
            AreAllSoftConstraintsSatisfied =
                ClinicalProtocolItemEvaluationResults
                    .Where(x => x.ClinicalProtocolItem.ConstraintType == ConstraintType.Soft)
                    .All(y => y.IsClinicalProtocolSatisfied);
            AreAllConstraintsSatisfied = AreAllSoftConstraintsSatisfied && AreAllHardConstraintsSatisfied;
        }

        protected bool Equals(ClinicalProtocolEvaluationResult other)
        {
            if (ClinicalProtocolItemEvaluationResults.Count() != other.ClinicalProtocolItemEvaluationResults.Count())
                return false;
            foreach (var item in ClinicalProtocolItemEvaluationResults)
                if (!other.ClinicalProtocolItemEvaluationResults.Contains(item))
                    return false;
            return AreAllSoftConstraintsSatisfied == other.AreAllSoftConstraintsSatisfied &&
                   AreAllHardConstraintsSatisfied == other.AreAllHardConstraintsSatisfied;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ClinicalProtocolEvaluationResult)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(AreAllConstraintsSatisfied, AreAllHardConstraintsSatisfied,
                AreAllSoftConstraintsSatisfied, ClinicalProtocolItemEvaluationResults);
        }
    }
}
