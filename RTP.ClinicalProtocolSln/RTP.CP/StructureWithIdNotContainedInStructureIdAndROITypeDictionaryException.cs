﻿using System;

namespace RTP.CP
{
    public class StructureWithIdNotContainedInStructureIdAndRoiTypeDictionaryException : Exception
    {
        public StructureWithIdNotContainedInStructureIdAndRoiTypeDictionaryException()
        {

        }

        public StructureWithIdNotContainedInStructureIdAndRoiTypeDictionaryException(string message) : base(message)
        {

        }
    }
}
