﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace RTP.CP
{
    public static class ClinicalProtocolSerializer
    {
        public static string Serialize(ClinicalProtocol clinicalProtocol)
            => JsonConvert.SerializeObject(clinicalProtocol, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            });

        public static ClinicalProtocol Deserialize(string serializedClinicalProtocol)
        {
            if (serializedClinicalProtocol == null) throw new ArgumentNullException(nameof(serializedClinicalProtocol));
            return JsonConvert.DeserializeObject<ClinicalProtocol>(serializedClinicalProtocol, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            });
        }

        public static bool WriteToFile(FileInfo fullFilePath, ClinicalProtocol clinicalProtocol)
        {
            try
            {
                File.WriteAllText(fullFilePath.FullName, Serialize(clinicalProtocol));
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static ClinicalProtocol ReadFromFile(FileInfo fullFilePath)
        {
            try
            {
                var content = File.ReadAllText(fullFilePath.FullName);
                return Deserialize(content);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
