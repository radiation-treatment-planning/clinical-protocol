﻿using System;
using System.Collections.Generic;
using System.Linq;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.ROIType;

namespace RTP.CP
{
    public class DvhCurveAndRoiTypeConfigBuilder
    {
        private StructureIdAndRoiTypeDictionary _StructureIdAndRoiTypeDictionary;
        private List<UDvhEstimateDetached> _dvhEstimates;

        internal DvhCurveAndRoiTypeConfigBuilder()
        {
            _dvhEstimates = new List<UDvhEstimateDetached>();
        }

        public static DvhCurveAndRoiTypeConfigBuilder Initialize()
            => new DvhCurveAndRoiTypeConfigBuilder();

        public DvhCurveAndRoiTypeConfigBuilder WithStructureIdAndRoiTypeDictionary(
            StructureIdAndRoiTypeDictionary structureIdAndRoiTypeDictionary)
        {
            _StructureIdAndRoiTypeDictionary = structureIdAndRoiTypeDictionary ??
                                               throw new ArgumentNullException(nameof(structureIdAndRoiTypeDictionary));
            return this;
        }

        public DvhCurveAndRoiTypeConfigBuilder WithDvhEstimates(IEnumerable<UDvhEstimateDetached> dvhEstimates)
        {
            if (dvhEstimates == null) throw new ArgumentNullException(nameof(dvhEstimates));
            _dvhEstimates.AddRange(dvhEstimates);
            return this;
        }

        /// <summary>
        /// Builds a DVHCurveAndROITypeConfig. If multiple DVH Estimates for the
        /// same structure are defined, only the first DVH Estimate will be
        /// considered.
        /// </summary>
        /// <returns>DVHCurveAndROITypeConfig</returns>
        public DVHCurveAndROITypeConfig Build()
        {
            CheckInput();
            var dvhCurveAndRoiTypeTuples = new List<DVHCurveAndROITypeTuple>(_dvhEstimates.Count);
            foreach (var dvhEstimate in _dvhEstimates)
            {
                var structureId = dvhEstimate.StructureId;
                RtRoiInterpretedType rtRoiInterpretedType;
                var success =
                    _StructureIdAndRoiTypeDictionary.TryGetROITypeOfStructureWithId(structureId,
                        out rtRoiInterpretedType);
                if (success)
                    dvhCurveAndRoiTypeTuples.Add(
                        new DVHCurveAndROITypeTuple(dvhEstimate.DvhCurve, rtRoiInterpretedType));
            }

            return new DVHCurveAndROITypeConfig(dvhCurveAndRoiTypeTuples);
        }

        private void CheckInput()
        {
            if (_StructureIdAndRoiTypeDictionary == null)
                throw new NullReferenceException($"There is no {nameof(StructureIdAndRoiTypeDictionary)} defined.");

            var dictionaryStructureIds = _StructureIdAndRoiTypeDictionary.GetAllStructureIds();
            var dvhEstimateStructureIds = _dvhEstimates.Select(x => x.StructureId);
            foreach (var structureId in dvhEstimateStructureIds)
            {
                if (!dictionaryStructureIds.Contains(structureId))
                    throw new StructureWithIdNotContainedInStructureIdAndRoiTypeDictionaryException(
                        $"Structure with ID {structureId} is not contained in DVH Estimates.");
            }
        }
    }
}
