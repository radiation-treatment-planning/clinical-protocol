﻿using System;
using RadiationTreatmentPlanner.Utils.Dose;

namespace RTP.CP
{
    public class ClinicalProtocolItemEvaluationResult
    {
        public bool IsClinicalProtocolSatisfied { get; }
        public IClinicalProtocolItem ClinicalProtocolItem { get; }
        public UDose ActualDose { get; }

        public ClinicalProtocolItemEvaluationResult(bool isClinicalProtocolSatisfied,
            IClinicalProtocolItem clinicalProtocolItem, UDose actualDose)
        {
            IsClinicalProtocolSatisfied = isClinicalProtocolSatisfied;
            ClinicalProtocolItem = clinicalProtocolItem;
            ActualDose = actualDose;
        }

        protected bool Equals(ClinicalProtocolItemEvaluationResult other)
        {
            return IsClinicalProtocolSatisfied == other.IsClinicalProtocolSatisfied &&
                   Equals(ClinicalProtocolItem, other.ClinicalProtocolItem) && Equals(ActualDose, other.ActualDose);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ClinicalProtocolItemEvaluationResult)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(IsClinicalProtocolSatisfied, ClinicalProtocolItem, ActualDose);
        }
    }
}
