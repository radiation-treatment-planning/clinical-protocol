﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RTP.CP
{
    public class ClinicalProtocolEvaluator
    {
        /// <summary>
        /// Evaluate whether a set of DVH curves satisfies a set of clinical
        /// protocol items. If the exists no DVH curve for a protocol item,
        /// it is considered to be satisfied.
        /// </summary>
        /// <param name="clinicalProtocol">The clinical protocol with its clinical constraints.</param>
        /// <param name="dvhCurveAndRoiTypeConfig">A configuration that maps DVH curves to
        /// RT ROI Interpreted types.</param>
        /// <returns>ClinicalProtocolEvaluationResult.</returns>
        public ClinicalProtocolEvaluationResult Evaluate(ClinicalProtocol clinicalProtocol,
            DVHCurveAndROITypeConfig dvhCurveAndRoiTypeConfig)
        {
            if (dvhCurveAndRoiTypeConfig == null) throw new ArgumentNullException(nameof(dvhCurveAndRoiTypeConfig));
            if (!dvhCurveAndRoiTypeConfig.DvhCurveAndRoiTypeTuples.Any())
                return new ClinicalProtocolEvaluationResult(Array.Empty<ClinicalProtocolItemEvaluationResult>());
            var clinicalProtocolItemEvaluationResults =
                new List<ClinicalProtocolItemEvaluationResult>(clinicalProtocol.ClinicalProtocolItems.Count());
            foreach (var clinicalProtocolItem in clinicalProtocol.ClinicalProtocolItems)
                clinicalProtocolItemEvaluationResults.Add(IsProtocolItemValid(clinicalProtocolItem,
                    dvhCurveAndRoiTypeConfig));

            return new ClinicalProtocolEvaluationResult(clinicalProtocolItemEvaluationResults);
        }

        private ClinicalProtocolItemEvaluationResult IsProtocolItemValid(IClinicalProtocolItem clinicalProtocolItem,
            DVHCurveAndROITypeConfig dvhCurveAndRoiTypeConfig)
        {
            if (clinicalProtocolItem == null) throw new ArgumentNullException(nameof(clinicalProtocolItem));
            if (dvhCurveAndRoiTypeConfig == null) throw new ArgumentNullException(nameof(dvhCurveAndRoiTypeConfig));

            var relevantDvhCurve = dvhCurveAndRoiTypeConfig.DvhCurveAndRoiTypeTuples.FirstOrDefault(x =>
                Equals(x.RoiInterpretedType, clinicalProtocolItem.RegionOfInterestType))?.DvhCurve;
            if (relevantDvhCurve == null)
                throw new NoDvhCurveForClinicalProtocolItemDefinedException(
                    $"No DVH Curve defined for structure with ROI Interpreted type {clinicalProtocolItem.RegionOfInterestType.Code}.");

            var (convertedClinicalProtocolItem, convertedDvhCurve) =
                clinicalProtocolItem.EquateVolumesOfProtocolItemAndDvhCurve(relevantDvhCurve);
            var evaluationResult = convertedClinicalProtocolItem.IsSatisfiedByDvhCurve(convertedDvhCurve);
            return new ClinicalProtocolItemEvaluationResult(evaluationResult.IsClinicalProtocolSatisfied,
                clinicalProtocolItem, evaluationResult.ActualDose);
        }
    }
}
